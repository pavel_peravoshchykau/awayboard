FROM openjdk:8-jre
WORKDIR /usr/local/awayboard
VOLUME ["/usr/local/awayboard"]
COPY build/libs/awayboard.jar /usr/local/awayboard/awayboard.jar
ENV PORT 8080
EXPOSE 8080
CMD ["java", "-jar", "/usr/local/awayboard/awayboard.jar"]