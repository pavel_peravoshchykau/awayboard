create table if not exists employees(
employee_id int,
employee_name varchar(255),
status_id int,
image_url varchar(255),
primary key (employee_id, employee_name));

create table if not exists teams(
team_id int,
team_name varchar(255),
image_url varchar(255),
primary key (team_id, team_name));

create table if not exists employee_teams(
employee_id int,
team_id int,
primary key (employee_id, team_id));

create table if not exists status(
status_id int,
status varchar(255),
primary key (status_id));

CREATE SEQUENCE id_sequence START 1;
