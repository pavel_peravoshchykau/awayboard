package awayboard.util;

import awayboard.dao.CommonDAO;
import awayboard.entity.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class MidnightUpdater {

    @Autowired
    private CommonDAO commonDAO;
    private LocalDate lastUpdateDate;

    @Scheduled(fixedDelay=10000)
    public void tryUpdate() {
        if (!LocalDate.now().equals(lastUpdateDate)) {
            commonDAO.updateEmployeesStatus(Status.AWAY);
            lastUpdateDate = LocalDate.now();
        }
    }
}
