package awayboard.controllers;

import awayboard.dao.CommonDAO;
import awayboard.entity.ErrorResponse;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Getter
public abstract class CommonService {
    @Autowired
    private CommonDAO commonDAO;

    protected ResponseEntity getErrorResponse(String message, HttpStatus status) {
        return new ResponseEntity<>(new ErrorResponse(message), status);
    }
}
