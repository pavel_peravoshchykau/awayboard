package awayboard.dao;

import awayboard.entity.Employee;
import awayboard.entity.Status;
import awayboard.entity.Team;

import java.util.List;

public interface CommonDAO {
    List<Team> getTeams();
    Team addTeam(String teamName, String imageUrl);
    Team getTeam(Integer teamId);
    void updateTeam(Team team);
    List<Employee> getEmployeesForTheTeam(Integer teamId);
    void addEmployeeToTheTeam(Integer employeeId, Integer teamId);
    void removeEmployeeFromTheTeam(Integer employeeId, Integer teamId);
    Employee addEmployee(String employeeName, String imageUrl);
    void updateEmployee(Employee employee);
    void updateEmployeeStatus(Integer employeeId, Status status);
    void updateEmployeesStatus(Status status);
}
