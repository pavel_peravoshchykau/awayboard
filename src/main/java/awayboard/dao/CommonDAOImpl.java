package awayboard.dao;

import awayboard.entity.Employee;
import awayboard.entity.Status;
import awayboard.entity.Team;
import awayboard.exceptions.DaoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class CommonDAOImpl implements CommonDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final RowMapper<Team> teamRowMapper = (rs, rowNum) -> Team.builder()
            .id(rs.getInt("team_id"))
            .name(rs.getString("team_name"))
            .imageUrl(rs.getString("image_url")).build();

    private final RowMapper<Employee> employeeRowMapper = (rs, rowNum) -> Employee.builder()
            .id(rs.getInt("employee_id"))
            .name(rs.getString("employee_name"))
            .status(Status.valueOf(rs.getString("status").toUpperCase()))
            .imageUrl(rs.getString("image_url")).build();

    @Override
    public List<Team> getTeams() {
        try {
            return jdbcTemplate.query("select * from teams", teamRowMapper);
        } catch (EmptyResultDataAccessException ex) {
            return Collections.emptyList();
        }  catch (Exception ex) {
            throw wrapSqlException(ex, "Failed get all teams");
        }
    }

    @Override
    public Team addTeam(String teamName, String imageUrl) {
        try {
            Integer id = getNexIdFromSequence();
            jdbcTemplate.update("insert into teams(team_id, team_name, image_url) values(?,?,?)", id, teamName, imageUrl);
            return Team.builder().id(id).name(teamName).imageUrl(imageUrl).build();
        } catch (Exception ex) {
            throw wrapSqlException(ex, "Failed to add new team");
        }
    }

    @Override
    public Team getTeam(Integer teamId) {
        try {
            return jdbcTemplate.queryForObject("select * from teams where team_id = ?", teamRowMapper, teamId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        } catch (Exception ex) {
            throw wrapSqlException(ex, "Failed to get team by id.");
        }
    }

    @Override
    public void updateTeam(Team team) {
        try {
            jdbcTemplate.update("update teams set team_name = ?, image_url = ? where team_id = ?",
                    team.getName(), team.getImageUrl(), team.getId());
        } catch (Exception ex) {
            throw wrapSqlException(ex, "Failed to update team. Check that team's id is correct.");
        }
    }

    @Override
    public List<Employee> getEmployeesForTheTeam(Integer teamId) {
        try {
            return jdbcTemplate.query("select et.employee_id, e.employee_name, s.status, e.image_url " +
                    " from employee_teams et " +
                    " inner join employees e on et.employee_id = e.employee_id " +
                    " inner join status s on e.status_id = s.status_id" +
                    " where et.team_id = ? ", employeeRowMapper, teamId);
        } catch (EmptyResultDataAccessException ex) {
            return Collections.emptyList();
        } catch (Exception ex) {
            throw wrapSqlException(ex, "Failed to get employees for the team. Check that team's id is correct.");
        }
    }

    @Override
    public void addEmployeeToTheTeam(Integer employeeId, Integer teamId) {
        try {
            jdbcTemplate.update("insert into employee_teams values(" +
                    " (select employee_id from employees where employee_id = ?)," +
                    " (select team_id from teams where team_id = ?))", employeeId, teamId);
        } catch (Exception ex) {
            throw wrapSqlException(ex, "Failed to add employee to the team. " +
                    "Check that employee's id and team's id are correct.");
        }
    }

    @Override
    public void removeEmployeeFromTheTeam(Integer employeeId, Integer teamId) {
        try {
            jdbcTemplate.update("delete from employee_teams where employee_id=? and team_id=?", employeeId, teamId);
        } catch (Exception ex) {
            throw wrapSqlException(ex, "Failed to remove employee from the team. " +
                    "Check that employee's id and team's id are correct.");
        }
    }

    @Override
    public Employee addEmployee(String employeeName, String imageUrl) {
        Integer id = getNexIdFromSequence();
        Employee employee = Employee.builder().id(id).name(employeeName).imageUrl(imageUrl).build();
        try {
            jdbcTemplate.update("insert into employees (employee_id, employee_name, status_id, image_url) values(?,?,?,?)",
                    id, employeeName, employee.getStatus().ordinal(), imageUrl);
        } catch (Exception ex) {
            throw wrapSqlException(ex, "Failed to add new employee");
        }
        return employee;
    }

    @Override
    public void updateEmployee(Employee employee) {
        try {
            jdbcTemplate.update("update employees set employee_name=?, status_id=?, image_url=? where employee_id=?",
                    employee.getName(), employee.getStatus(), employee.getImageUrl(), employee.getId());
        } catch (Exception ex) {
            throw wrapSqlException(ex, "Failed to update employee. Check that employee's id is correct.");
        }
    }

    @Override
    public void updateEmployeeStatus(Integer employeeId, Status status) {
        try {
            jdbcTemplate.update("update employees set status_id=?, where employee_id=?", status.ordinal(), employeeId);
        } catch (Exception ex) {
            throw wrapSqlException(ex, "Failed to update employee's status. Check that employee's id is correct.");
        }

    }

    @Override
    public void updateEmployeesStatus(Status status) {
        try {
            jdbcTemplate.update("update employees set status_id=?", status.ordinal());
        } catch (Exception ex) {
            throw wrapSqlException(ex, "Failed to update employee status for all employees.");
        }
    }

    private int getNexIdFromSequence() {
        return jdbcTemplate.queryForObject("SELECT nextval('id_sequence')", Integer.class);
    }

    private DaoException wrapSqlException(Exception ex, String message) {
        log.error(message, ex);
        if (ex instanceof SQLException || ex.getCause() instanceof SQLException) {
            return new DaoException(MessageFormat.format("{0}. Thrown: {1}. Check logs for more details.", message, ex.getClass().getSimpleName()));
        }
        return new DaoException(message, ex);
    }
}
