package awayboard.entity;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Team {
    private int id;
    private String name;
    private String imageUrl;
}
