package awayboard.entity;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private Integer id;
    private String name;
    @Builder.Default
    private Status status = Status.AWAY;
    private String imageUrl;
}
