package awayboard.entity;

public enum Status {
    OFFICE, HOMEOFFICE, AWAY
}
